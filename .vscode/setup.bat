copy /Y "masp\frontend\package.json" "masp\example-module\frontend\package.json"
copy /Y "masp\frontend\package.json" "users\frontend\package.json"
copy /Y "masp\frontend\package.json" "tasks\frontend\package.json"
cd masp\frontend
call npm install
cd ..
cd ..
cd masp\example-module\frontend
call npm install
cd ..
cd ..
cd ..
cd users\frontend
call npm install
cd ..
cd ..
cd tasks\frontend
call npm install
cd ..
cd ..
call go get ./masp/cmd
