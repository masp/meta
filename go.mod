module git.mo-mar.de/masp

go 1.12

require (
	github.com/coreos/go-semver v0.3.0
	github.com/gin-gonic/gin v1.3.1-0.20190418024537-f9de6049cbf0
	github.com/gocarina/gocsv v0.0.0-20190426105157-2fc85fcf0c07
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/json-iterator/go v1.1.6 // indirect
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b // indirect
	github.com/moqmar/parcel-serve v0.0.0-20181210154121-7f17008063b5
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/pengux/check v0.0.0-20190311071206-d71bc79a3654
	github.com/pkg/errors v0.8.1 // indirect
	github.com/pressly/goose v2.6.0+incompatible
	github.com/sirupsen/logrus v1.4.1
	github.com/x-cray/logrus-prefixed-formatter v0.5.2
	golang.org/x/crypto v0.0.0-20190418165655-df01cb2cc480
	golang.org/x/net v0.0.0-20190419010253-1f3472d942ba // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
